import config from 'config';
import { leaguestatsConstants } from '../_constants';
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const leaguestatsActions = {
    fetchLeagueStats
};

function fetchLeagueStats(value) {
    return dispatch => {
        dispatch(fetchStatsBegin());
        return fetch(`${config.apiUrl}/api/standings/FetchStandingsInfo?league=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchStatsSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchStatsFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const fetchStatsBegin = () => ({
    type: leaguestatsConstants.FETCH_LEAGUESTATS_BEGIN
});

export const fetchStatsSuccess = teams => ({
    type: leaguestatsConstants.FETCH_LEAGUESTATS_SUCCESS,
    payload: { teams }
});

export const fetchStatsFailure = error => ({
    type: leaguestatsConstants.FETCH_LEAGUESTATS_FAILURE,
    payload: { error }
});