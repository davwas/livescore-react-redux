import config from 'config';
import { playerConstants } from '../_constants';
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const playerActions = {
    fetchPlayer
};

function fetchPlayer(value) {
    return dispatch => {
        dispatch(fetchPlayerBegin());
        return fetch(`${config.apiUrl}/api/players/FetchPlayerInfo?player_id=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchPlayerSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchPlayerFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const fetchPlayerBegin = () => ({
    type: playerConstants.FETCH_PLAYER_BEGIN
});

export const fetchPlayerSuccess = player => ({
    type: playerConstants.FETCH_PLAYER_SUCCESS,
    payload: { player }
});

export const fetchPlayerFailure = error => ({
    type: playerConstants.FETCH_PLAYER_FAILURE,
    payload: { error }
});