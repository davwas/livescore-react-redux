import config from 'config';
import { assistscorersConstants } from '../_constants';
import { authHeader } from '../_helpers';
import { userService } from '../_services'
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const assistscorersActions = {
    fetchAssistscorersStats
};

function fetchAssistscorersStats(value) {
    return dispatch => {
        dispatch(fetchAssistscorersBegin());
        return fetch(`${config.apiUrl}/api/topscorers/FetchAssistscorersInfo?league=${value}`, {method: 'GET', headers: authHeader()})
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchAssistscorersSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchAssistscorersFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        if (response.status === 401) {
            userService.logout();
        }
        throw Error(response.statusText);
    }
    return response;
}

export const fetchAssistscorersBegin = () => ({
    type: assistscorersConstants.FETCH_ASSISTSCORERS_BEGIN
});

export const fetchAssistscorersSuccess = topassistants => ({
    type: assistscorersConstants.FETCH_ASSISTSCORERS_SUCCESS,
    payload: { topassistants }
});

export const fetchAssistscorersFailure = error => ({
    type: assistscorersConstants.FETCH_ASSISTSCORERS_FAILURE,
    payload: { error }
});