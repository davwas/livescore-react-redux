import config from 'config';
import { goalscorersConstants } from '../_constants';
import { authHeader } from '../_helpers';
import { userService } from '../_services'
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const goalscorersActions = {
    fetchGoalscorersStats
};

function fetchGoalscorersStats(value) {
    return dispatch => {
        dispatch(fetchGoalscorersBegin());
        return fetch(`${config.apiUrl}/api/topscorers/FetchgoalscorersInfo?league=${value}`, {method: 'GET', headers: authHeader()})
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchGoalscorersSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchGoalscorersFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        if (response.status === 401) {
            userService.logout();
        }
        throw Error(response.statusText);
    }
    return response;
}

export const fetchGoalscorersBegin = () => ({
    type: goalscorersConstants.FETCH_GOALSCORERS_BEGIN
});

export const fetchGoalscorersSuccess = topscorers => ({
    type: goalscorersConstants.FETCH_GOALSCORERS_SUCCESS,
    payload: { topscorers }
});

export const fetchGoalscorersFailure = error => ({
    type: goalscorersConstants.FETCH_GOALSCORERS_FAILURE,
    payload: { error }
});