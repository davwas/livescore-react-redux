import config from 'config';
import { head2headConstants } from '../_constants';
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const head2headActions = {
    fetchH2H
};

function fetchH2H(value1, value2) {
    return dispatch => {
        dispatch(fetchH2HBegin());
        return fetch(`${config.apiUrl}/api/head2head/FetchHead2HeadInfo?hometeamID=${value1}&awayteamID=${value2}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchH2HSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchH2HFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const fetchH2HBegin = () => ({
    type: head2headConstants.FETCH_H2H_BEGIN
});

export const fetchH2HSuccess = h2h => ({
    type: head2headConstants.FETCH_H2H_SUCCESS,
    payload: { h2h }
});

export const fetchH2HFailure = error => ({
    type: head2headConstants.FETCH_H2H_FAILURE,
    payload: { error }
});