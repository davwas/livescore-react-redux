import config from 'config';
import { teamConstants } from '../_constants';
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const teamActions = {
    fetchTeam
};

function fetchTeam(value) {
    return dispatch => {
        dispatch(fetchTeamBegin());
        return fetch(`${config.apiUrl}/api/teams/FetchTeamInfo?team_id=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchTeamSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchTeamFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const fetchTeamBegin = () => ({
    type: teamConstants.FETCH_TEAM_BEGIN
});

export const fetchTeamSuccess = team => ({
    type: teamConstants.FETCH_TEAM_SUCCESS,
    payload: { team }
});

export const fetchTeamFailure = error => ({
    type: teamConstants.FETCH_TEAM_FAILURE,
    payload: { error }
});