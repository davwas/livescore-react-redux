import config from 'config';
import { fixturesConstants } from '../_constants';

export const fixturesActions = {
    fetchAllMatchesInitial,
    fetchAllMatches,
    fetchLiveMatchesInitial,
    fetchLiveMatches,
    fetchEndedMatchesInitial,
    fetchEndedMatches
};

function fetchAllMatchesInitial(value) {
    return dispatch => {
        dispatch(fetchMatchesInitialBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchAllMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesInitialSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesInitialFailure(error)));
    };
}

function fetchAllMatches(value) {
    return dispatch => {
        dispatch(fetchMatchesBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchAllMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesFailure(error)));
    };
}

function fetchLiveMatchesInitial(value) {
    return dispatch => {
        dispatch(fetchMatchesInitialBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchLiveMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesInitialSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesInitialFailure(error)));
    };
}

function fetchLiveMatches(value) {
    return dispatch => {
        dispatch(fetchMatchesBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchLiveMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesFailure(error)));
    };
}

function fetchEndedMatchesInitial(value) {
    return dispatch => {
        dispatch(fetchMatchesInitialBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchEndedMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesInitialSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesInitialFailure(error)));
    };
}
function fetchEndedMatches(value) {
    return dispatch => {
        dispatch(fetchMatchesBegin());
        return fetch(`${config.apiUrl}/api/fixtures/FetchEndedMatches?date=${value}`)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchMatchesSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchMatchesFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const fetchMatchesBegin = () => ({
    type: fixturesConstants.FETCH_MATCHES_BEGIN
});

export const fetchMatchesSuccess = matches => ({
    type: fixturesConstants.FETCH_MATCHES_SUCCESS,
    payload: { matches }
});

export const fetchMatchesFailure = error => ({
    type: fixturesConstants.FETCH_MATCHES_FAILURE,
    payload: { error }
});

export const fetchMatchesInitialBegin = () => ({
    type: fixturesConstants.FETCH_MATCHES_INITIAL_BEGIN
});

export const fetchMatchesInitialSuccess = matches => ({
    type: fixturesConstants.FETCH_MATCHES_INITIAL_SUCCESS,
    payload: { matches }
});

export const fetchMatchesInitialFailure = error => ({
    type: fixturesConstants.FETCH_MATCHES_INITIAL_FAILURE,
    payload: { error }
});