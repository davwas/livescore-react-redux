import React from 'react';
import { Router, Route, withRouter, Redirect, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { Header, Footer } from '../_components';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { FixturesPage } from '../FixturesPage';
import { TeamPage } from '../TeamPage';
import './app.css';
import { selectConstants, matchtypeConstants } from '../_constants';
import { dateOptions } from '../_helpers';
import { StatsPage } from '../StatsPage';
import { NoMatchPage } from '../NoMatchPage/NoMatchPage';
import { PlayerPage } from '../PlayerPage';

const RouteNotFound = () => <Redirect to={{ state: { notFoundError: true } }} />;
const CaptureRouteNotFound = withRouter(({ children, location }) => {
    return location && location.state && location.state.notFoundError ? <NoMatchPage /> : children;
});

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
            dispatch({
                type: selectConstants.UPDATE_SELECT_OPTION,
                payload: dateOptions[7]
            });
            dispatch({
                type: matchtypeConstants.MATCH_TYPE_ALL
            });
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <Router history={history}>
                <div className="flex-wrapper">
                    <Header />
                    <div className="content">
                        <CaptureRouteNotFound>
                            <Switch>
                                <Route path="/login" component={LoginPage} />
                                <Route path="/register" component={RegisterPage} />
                                <Route exact path="/" component={FixturesPage} />
                                <Route path="/stats" component={StatsPage} />
                                <Route path="/team/:id" component={TeamPage} />
                                <Route path="/player/:pid" component={PlayerPage} />
                                <RouteNotFound />
                            </Switch>
                        </CaptureRouteNotFound>
                    </div>
                    <Footer />
                </div>
            </Router>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 