import React from 'react';
import { connect } from 'react-redux';
import { leaguestatsActions, goalscorersActions, assistscorersActions } from '../_actions';
import Loader from 'react-loader-spinner';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import FlagIcon from '../_components/FlagIcon';
import './statspage.css';

class StatsCountry extends React.Component {
    constructor(props) {
        super(props);
        if (location.pathname === "/stats/premierleague") {
            this.state = {
                League: "Premier League"
            }
        }
        if (location.pathname === "/stats/bundesliga") {
            this.state = {
                League: "Bundesliga"
            }
        }
        if (location.pathname === "/stats/ligueone") {
            this.state = {
                League: "Ligue 1"
            }
        }
        if (location.pathname === "/stats/laliga") {
            this.state = {
                League: "La Liga"
            }
        }
        if (location.pathname === "/stats/seriea") {
            this.state = {
                League: "Serie A"
            }
        }
    }
    componentDidMount() {
        if (!this.props.loading) {
            this.props.dispatch(leaguestatsActions.fetchLeagueStats(this.state.League));
            this.props.dispatch(goalscorersActions.fetchGoalscorersStats(this.state.League));
            this.props.dispatch(assistscorersActions.fetchAssistscorersStats(this.state.League));
        }
    }
    render() {
        const { teams, error, loading, topscorers, topassistants } = this.props;
        const { League } = this.state;
        const { getCode } = require('country-list');
        const { loggedIn } = this.props;
        if (error) {
            return <div className="loader text-center">Błąd API! {error.message}</div>;
        }
        if (loading) {
            return <div className="loader text-center">
                <Loader
                    type="Triangle"
                    color="#006400"
                    height="100"
                    width="100"
                />
            </div>;
        }
        return (
            <div>
                <Table hover responsive>
                    <thead>
                        <tr>
                            <th colSpan={11} className="text-center">{League} - Ranking</th>
                        </tr>
                        <tr>
                            <th className="country-stats-header first text-center position">#</th>
                            <th className="country-stats-header">Klub</th>
                            <th className="country-stats-header text-center form-column">Forma</th>
                            <th className="country-stats-header text-center" title="Rozegrane mecze">RM</th>
                            <th className="country-stats-header text-center" title="Wygrana">W</th>
                            <th className="country-stats-header text-center" title="Remis">R</th>
                            <th className="country-stats-header text-center" title="Przegrana">P</th>
                            <th className="country-stats-header text-center" title="Bramki zdobyte">BZ</th>
                            <th className="country-stats-header text-center" title="Bramki stracone">BS</th>
                            <th className="country-stats-header text-center" title="Różnica bramek">RB</th>
                            <th className="country-stats-header last text-center" title="Punkty">Pkt</th>
                        </tr>
                    </thead>
                    <tbody>
                        {teams.map((team, index) => (
                            team.result && team.result.includes("Champions League") ?
                                <tr className="team-cl" key={index}>
                                    <td className="text-center"><strong>{index + 1}</strong></td>
                                    <td><img className="country-team-logo" title={team.team_name} src={team.logo_path} /><Link to={`/team/${team.team_id}`} className="team-link">{team.team_name}</Link></td>
                                    <td className="text-center">
                                        {team.formArray.map((result, index_result) => (
                                            result === "W" ? <div key={index_result} className="form-win" title="Wygrana">W</div> : result === "D" ? <div key={index_result} className="form-draw" title="Remis">R</div> : <div key={index_result} className="form-loss" title="Przegrana">P</div>
                                        ))}
                                    </td>
                                    <td className="text-center">{team.games_played}</td>
                                    <td className="text-center">{team.won}</td>
                                    <td className="text-center">{team.draw}</td>
                                    <td className="text-center">{team.lost}</td>
                                    <td className="text-center">{team.goals_scored}</td>
                                    <td className="text-center">{team.goals_against}</td>
                                    <td className="text-center">{team.goals_scored - team.goals_against}</td>
                                    <td className="text-center">{team.points}</td>
                                </tr> :
                                team.result && team.result.includes("Europa") ?
                                    <tr className="team-el" key={index}>
                                        <td className="text-center"><strong>{index + 1}</strong></td>
                                        <td><img className="country-team-logo" title={team.team_name} src={team.logo_path} /><Link to={`/team/${team.team_id}`} className="team-link">{team.team_name}</Link></td>
                                        <td className="text-center">
                                            {team.formArray.map((result, index_result) => (
                                                result === "W" ? <div key={index_result} className="form-win" title="Wygrana">W</div> : result === "D" ? <div key={index_result} className="form-draw" title="Remis">R</div> : <div key={index_result} className="form-loss" title="Przegrana">P</div>
                                            ))}
                                        </td>
                                        <td className="text-center">{team.games_played}</td>
                                        <td className="text-center">{team.won}</td>
                                        <td className="text-center">{team.draw}</td>
                                        <td className="text-center">{team.lost}</td>
                                        <td className="text-center">{team.goals_scored}</td>
                                        <td className="text-center">{team.goals_against}</td>
                                        <td className="text-center">{team.goals_scored - team.goals_against}</td>
                                        <td className="text-center">{team.points}</td>
                                    </tr> :
                                    team.result && team.result.includes("Relegation") ?
                                        <tr className="team-relegation" key={index}>
                                            <td className="text-center"><strong>{index + 1}</strong></td>
                                            <td><img className="country-team-logo" title={team.team_name} src={team.logo_path} /><Link to={`/team/${team.team_id}`} className="team-link">{team.team_name}</Link></td>
                                            <td className="text-center">
                                                {team.formArray.map((result, index_result) => (
                                                    result === "W" ? <div key={index_result} className="form-win" title="Wygrana">W</div> : result === "D" ? <div key={index_result} className="form-draw" title="Remis">R</div> : <div key={index_result} className="form-loss" title="Przegrana">P</div>
                                                ))}
                                            </td>
                                            <td className="text-center">{team.games_played}</td>
                                            <td className="text-center">{team.won}</td>
                                            <td className="text-center">{team.draw}</td>
                                            <td className="text-center">{team.lost}</td>
                                            <td className="text-center">{team.goals_scored}</td>
                                            <td className="text-center">{team.goals_against}</td>
                                            <td className="text-center">{team.goals_scored - team.goals_against}</td>
                                            <td className="text-center">{team.points}</td>
                                        </tr> :
                                        <tr key={index}>
                                            <td className="text-center"><strong>{index + 1}</strong></td>
                                            <td><img className="country-team-logo" title={team.team_name} src={team.logo_path} /><Link to={`/team/${team.team_id}`} className="team-link">{team.team_name}</Link></td>
                                            <td className="text-center">
                                                {team.formArray.map((result, index_result) => (
                                                    result === "W" ? <div key={index_result} className="form-win" title="Wygrana">W</div> : result === "D" ? <div key={index_result} className="form-draw" title="Remis">R</div> : <div key={index_result} className="form-loss" title="Przegrana">P</div>
                                                ))}
                                            </td>
                                            <td className="text-center">{team.games_played}</td>
                                            <td className="text-center">{team.won}</td>
                                            <td className="text-center">{team.draw}</td>
                                            <td className="text-center">{team.lost}</td>
                                            <td className="text-center">{team.goals_scored}</td>
                                            <td className="text-center">{team.goals_against}</td>
                                            <td className="text-center">{team.goals_scored - team.goals_against}</td>
                                            <td className="text-center">{team.points}</td>
                                        </tr>
                        ))}
                    </tbody>
                </Table>
                {loggedIn ?
                    <div>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={6} className="text-center">{League} - Najlepsi strzelcy</th>
                                </tr>
                                <tr>
                                    <th className="country-stats-header first text-center position"><strong>#</strong></th>
                                    <th className="country-stats-header">Zawodnik</th>
                                    <th className="country-stats-header text-center">Wiek</th>
                                    <th className="country-stats-header text-center">Klub</th>
                                    <th className="country-stats-header text-center">Bramki</th>
                                    <th className="country-stats-header last text-center">Rzuty karne</th>
                                </tr>
                            </thead>
                            <tbody>
                                {topscorers.map((gs, index2) => (
                                    <tr key={index2}>
                                        <td className="text-center player-column-1"><strong>{index2 + 1}</strong></td>
                                        <td className="player-column-2"><div className="cell-inline" title={gs.nationality}><FlagIcon code={getCode(gs.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<div className="cell-inline"><Link to={`/player/${gs.player_id}`} className="team-link">{gs.common_name}</Link></div></td>
                                        <td className="text-center player-column-3">{gs.age}</td>
                                        <td className="text-center player-column-4"><img className="player-team-logo" src={gs.logo_path} title={gs.name} /></td>
                                        <td className="text-center player-column-5">{gs.goals}</td>
                                        <td className="text-center player-column-6">{gs.penalty_goals}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={6} className="text-center">{League} - Najlepsi asystenci</th>
                                </tr>
                                <tr>
                                    <th className="country-stats-header first text-center position"><strong>#</strong></th>
                                    <th className="country-stats-header">Zawodnik</th>
                                    <th className="country-stats-header text-center">Wiek</th>
                                    <th className="country-stats-header text-center">Klub</th>
                                    <th className="country-stats-header last text-center">Asysty</th>
                                </tr>
                            </thead>
                            <tbody>
                                {topassistants.map((as, index3) => (
                                    <tr key={index3}>
                                        <td className="text-center player-column-1"><strong>{index3 + 1}</strong></td>
                                        <td className="player-column-2"><div className="cell-inline" title={as.nationality}><FlagIcon code={getCode(as.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<div className="cell-inline"><Link to={`/player/${as.player_id}`} className="team-link">{as.common_name}</Link></div></td>
                                        <td className="text-center player-column-3">{as.age}</td>
                                        <td className="text-center player-column-4"><img className="player-team-logo" src={as.logo_path} title={as.name} /></td>
                                        <td className="text-center player-column-5">{as.assists}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div> :
                    <div className="text-center"><h1>Zaloguj się aby zobaczyć więcej statystyk.</h1></div>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { teams, error, loading } = state.leaguestats;
    const { topscorers } = state.goalscorers;
    const { topassistants } = state.assistscorers;
    const { user, loggedIn } = state.authentication;
    return {
        teams,
        error,
        loading,
        topscorers,
        topassistants,
        user,
        loggedIn
    };
}

const connectedStatsCountry = connect(mapStateToProps)(StatsCountry);
export { connectedStatsCountry as StatsCountry }; 