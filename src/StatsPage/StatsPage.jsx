import React from 'react';
import { connect } from 'react-redux';
import { Alert, Grid, Row, Col, ListGroup, ListGroupItem } from 'react-bootstrap';
import { Link, Route } from 'react-router-dom';
import FlagIcon from '../_components/FlagIcon';
import { StatsCountry } from './StatsCountry';
import './statspage.css';

class StatsPage extends React.Component {
    render() {
        const { match, location } = this.props;
        return (
            <Grid className="stats-content">
                <Row>
                    <Col xs={12} xsOffset={0} sm={3} smOffset={0} md={2} mdOffset={0}>
                        <div>
                            <ListGroup className="stats-menu">
                                <ListGroupItem className="stats-menu-country"><FlagIcon code="gb-eng" />&nbsp;Anglia</ListGroupItem>
                                <ListGroupItem className="stats-menu-league"><img className="stats-menu-logo" src="https://cdn.sportmonks.com/images/soccer/leagues/8.png" />&nbsp;<Link to={`${match.url}/premierleague`}>Premier League</Link></ListGroupItem>
                                <ListGroupItem className="stats-menu-country"><FlagIcon code="fr" />&nbsp;Francja</ListGroupItem>
                                <ListGroupItem className="stats-menu-league"><img className="stats-menu-logo" src="https://cdn.sportmonks.com/images/soccer/leagues/301.png" />&nbsp;<Link to={`${match.url}/ligueone`}>Ligue 1</Link></ListGroupItem>
                                <ListGroupItem className="stats-menu-country"><FlagIcon code="es" />&nbsp;Hiszpania</ListGroupItem>
                                <ListGroupItem className="stats-menu-league"><img className="stats-menu-logo" src="https://cdn.sportmonks.com/images/soccer/leagues/564.png" />&nbsp;<Link to={`${match.url}/laliga`}>La Liga</Link></ListGroupItem>
                                <ListGroupItem className="stats-menu-country"><FlagIcon code="de" />&nbsp;Niemcy</ListGroupItem>
                                <ListGroupItem className="stats-menu-league"><img className="stats-menu-logo" src="https://cdn.sportmonks.com/images/soccer/leagues/82.png" />&nbsp;<Link to={`${match.url}/bundesliga`}>Bundesliga</Link></ListGroupItem>
                                <ListGroupItem className="stats-menu-country"><FlagIcon code="it" />&nbsp;Włochy</ListGroupItem>
                                <ListGroupItem className="stats-menu-league"><img className="stats-menu-logo" src="https://cdn.sportmonks.com/images/soccer/leagues/384.png" />&nbsp;<Link to={`${match.url}/seriea`}>Serie A</Link></ListGroupItem>
                            </ListGroup>
                            <div>
                                <div><strong>Legenda:</strong></div>
                                <Alert bsStyle="success">Liga Mistrzów</Alert>
                                <Alert>Liga Europy</Alert>
                                <Alert bsStyle="danger">Relegacja</Alert>
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} xsOffset={0} sm={9} smOffset={0} md={10} mdOffset={0}>
                        {location.pathname === "/stats" ? <div className="text-center">Wybierz ligę.</div> : ""}
                        <Route path={`${match.path}/premierleague`} component={StatsCountry} />
                        <Route path={`${match.path}/ligueone`} component={StatsCountry} />
                        <Route path={`${match.path}/laliga`} component={StatsCountry} />
                        <Route path={`${match.path}/bundesliga`} component={StatsCountry} />
                        <Route path={`${match.path}/seriea`} component={StatsCountry} />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { option, label } = state.select;
    const { matchesType } = state.matchtype;
    return {
        option,
        label,
        matchesType
    };
}

const connectedStatsPage = connect(mapStateToProps)(StatsPage);
export { connectedStatsPage as StatsPage }; 