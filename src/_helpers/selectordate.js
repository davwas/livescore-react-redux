const currentDateL = new Date();
const currentDateV = new Date();

function getAllLabelDates(date) {
    let list = [];
    date.setDate(date.getDate() - 7);
    let t = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
    list.push(t);
    var i;
    for (i = 0; i < 14; i++) {
        date.setDate(date.getDate() + 1);
        t = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
        list.push(t);
    };
    return list;
}

function getAllValueDates(date) {
    let list = [];
    date.setDate(date.getDate() - 7);
    let t = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    list.push(t);
    var i;
    for (i = 0; i < 14; i++) {
        date.setDate(date.getDate() + 1);
        t = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        list.push(t);
    };
    return list;
}

const labelList = getAllLabelDates(currentDateL);
const valueList = getAllValueDates(currentDateV);

export const dateOptions = [
    { value: valueList[0], label: labelList[0] },
    { value: valueList[1], label: labelList[1] },
    { value: valueList[2], label: labelList[2] },
    { value: valueList[3], label: labelList[3] },
    { value: valueList[4], label: labelList[4] },
    { value: valueList[5], label: labelList[5] },
    { value: valueList[6], label: labelList[6] },
    { value: valueList[7], label: "Dzisiaj" },
    { value: valueList[8], label: labelList[8] },
    { value: valueList[9], label: labelList[9] },
    { value: valueList[10], label: labelList[10] },
    { value: valueList[11], label: labelList[11] },
    { value: valueList[12], label: labelList[12] },
    { value: valueList[13], label: labelList[13] },
    { value: valueList[14], label: labelList[14] },
];