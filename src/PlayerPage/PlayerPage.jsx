import React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import FlagIcon from '../_components/FlagIcon';
import './playerpage.css';
import { playerActions } from '../_actions';

class PlayerPage extends React.Component {
    componentDidMount() {
        if (!this.props.loading) {
            this.props.dispatch(playerActions.fetchPlayer(this.props.match.params.pid));
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.props.dispatch(playerActions.fetchPlayer(this.props.match.params.pid));
        }
    }
    render() {
        const { player, error, loading } = this.props;
        const { getCode } = require('country-list');
        if (error) {
            return <div className="loader text-center">Błąd API! {error.message}</div>;
        }
        if (loading) {
            return <div className="loader text-center">
                <Loader
                    type="Triangle"
                    color="#006400"
                    height="100"
                    width="100"
                />
            </div>;
        }
        return (
            <Grid>
                <Row className="table-row">
                    <Col xs={5} xsOffset={0} md={2} mdOffset={3}>
                        <img className="player-image" src={player.image_path} />
                    </Col>
                    <Col xs={7} xsOffset={0} md={4} mdOffset={0}>
                        <h2 className="team-name">{player.nationality && <div className="player-page-flag" title={player.nationality}><FlagIcon code={getCode(player.nationality).toLowerCase()} /></div>} {player.firstname} {player.lastname}</h2>
                        <div className="font-16">Pozycja:&nbsp;
                        {player.position_name === "Goalkeeper" ? <strong>Bramkarz</strong> : player.position_name === "Defender" ? <strong>Obrońca</strong> :
                                player.position_name === "Midfielder" ? <strong>Pomocnik</strong> : <strong>Napastnik</strong>}
                        </div>
                        <div className="font-16">Data urodzenia: <strong>{player.birthdate}</strong><div className="inline venue-capacity">&nbsp;({player.age} lat)</div></div>
                        <div className="font-16">Wzrost: <strong>{player.height}</strong></div>
                        <div className="font-16">Waga: <strong>{player.weight}</strong></div>
                    </Col>
                </Row>
                <Row className="table-row">
                    <Col xs={7} xsOffset={0} md={4} mdOffset={3}>
                        <h2 className="team-name"><Link to={`/team/${player.team_id}`} className="team-link">{player.team_name}</Link></h2>
                        <div className="font-16">Mecze: <strong>{player.appearences}</strong></div>
                        <div className="font-16">Minut: <strong>{player.minutes}</strong></div>
                        <div className="font-16">Bramki: <strong>{player.goals}</strong></div>
                        <div className="font-16">Żółte kartki: <strong>{player.yellowcards}</strong></div>
                        <div className="font-16">Czerwone kartki: <strong>{player.redcards}</strong></div>
                        {player.minutes === 0 ? <div className="font-16"></div> : <div className="font-16">Bramek na 90 minut: <strong>{(Math.round(player.goals * 100 / (player.minutes / 90)) / 100).toString()}</strong></div>}
                    </Col>
                    <Col xs={5} xsOffset={0} md={2} mdOffset={0}>
                        <img className="player-page-logo" src={player.logo_path} />
                    </Col>
                </Row>
                <Row className="table-row-long">
                    <Col xs={12} xsOffset={0} md={8} mdOffset={2}>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={3} className="text-center">Zawieszenia i kontuzje</th>
                                </tr>
                                <tr>
                                    <th className="text-center">Od</th>
                                    <th className="text-center">Do</th>
                                    <th className="text-center">Rodzaj</th>
                                </tr>
                            </thead>
                            <tbody>
                                {player.injuries && player.injuries.length === 0 &&
                                    <tr><td colSpan={3} className="text-center">Brak kontuzji.</td></tr>}
                                {player.injuries && player.injuries.length !== 0 && player.injuries.map((injury, index) => (
                                    <tr key={index}>
                                        <td className="text-center time">{injury.start_date}</td>
                                        <td className="text-center time">{injury.end_date}</td>
                                        <td className="text-center">{injury.description}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { player, loading, error } = state.player;
    return {
        player,
        loading,
        error
    };
}

const connectedPlayerPage = connect(mapStateToProps)(PlayerPage);
export { connectedPlayerPage as PlayerPage }; 