import React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import FlagIcon from '../_components/FlagIcon';
import './teampage.css';
import { teamActions } from '../_actions';

class TeamPage extends React.Component {
    componentDidMount() {
        if (!this.props.loading) {
            this.props.dispatch(teamActions.fetchTeam(this.props.match.params.id));
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.props.dispatch(teamActions.fetchTeam(this.props.match.params.id));
        }
    }
    render() {
        const { team, error, loading } = this.props;
        const { getCode } = require('country-list');
        const yellowcard = require("../_static/yellowcard.svg");
        const redcard = require("../_static/redcard.svg");
        if (error) {
            return <div className="loader text-center">Błąd API! {error.message}</div>;
        }
        if (loading) {
            return <div className="loader text-center">
                <Loader
                    type="Triangle"
                    color="#006400"
                    height="100"
                    width="100"
                />
            </div>;
        }
        return (
            <Grid>
                <Row className="table-row">
                    <Col xs={6} xsOffset={0} md={2} mdOffset={1}>
                        <div>
                            <img className="team-page-logo" src={team.logo_path} />
                        </div>
                    </Col>
                    {team.venue && team.coach && team.coach.nationality &&
                        <Col xs={6} xsOffset={0} md={5} mdOffset={0}>
                            <div>
                                <h2 className="team-name">{team.name}</h2>
                                <div>Rok założenia: <strong>{team.founded}</strong></div>
                                <div>Stadion: <strong>{team.venue.name}</strong>&nbsp;<div className="inline venue-capacity">({team.venue.capacity} miejsc)</div></div>
                                <h3>Trener</h3>
                                <div><div className="cell-inline" title={team.coach.nationality}><FlagIcon code={getCode(team.coach.nationality).toLowerCase()} /></div>&nbsp;<strong>{team.coach.firstname}&nbsp;{team.coach.lastname}</strong></div>
                                <div>Wiek: <strong>{team.coach.age}</strong></div>
                            </div>
                        </Col>}
                </Row>
                <Row className="table-row">
                    <Col xs={12} xsOffset={0} md={8} mdOffset={2}>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={4} className="text-center">Ostatnie mecze</th>
                                </tr>
                            </thead>
                            <tbody>
                                {team.latestGames && team.latestGames.length !== 0 && team.latestGames.map((game, index) => (
                                    <tr key={index}>
                                        <td className="games-date">{game.date}</td>
                                        <td className="text-right team-home team-short">{game.matchWinner === game.homeTeam.name ? <strong><Link to={`/team/${game.localteam_id}`} className="team-link">{game.homeTeam.name}</Link></strong> : <Link to={`/team/${game.localteam_id}`} className="team-link">{game.homeTeam.name}</Link>}</td>
                                        <td className="text-center team-scores"><strong>{game.homeTeam.localteam_score} - {game.awayTeam.visitorteam_score}</strong></td>
                                        <td className="text-left team-away">{game.matchWinner === game.awayTeam.name ? <strong><Link to={`/team/${game.visitorteam_id}`} className="team-link">{game.awayTeam.name}</Link></strong> : <Link to={`/team/${game.visitorteam_id}`} className="team-link">{game.awayTeam.name}</Link>}</td>
                                    </tr>))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row className="table-row">
                    <Col xs={12} xsOffset={0} md={8} mdOffset={2}>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={7} className="text-center">Skład drużyny</th>
                                </tr>
                                <tr>
                                    <th className="text-center" title="Numer koszulki">Nr koszulki</th>
                                    <th className="text-center">Zawodnik</th>
                                    <th className="text-center">Wiek</th>
                                    <th className="text-center">Bramki</th>
                                    <th className="text-center">Asysty</th>
                                    <th className="text-center"><img src={yellowcard} title="Żółte kartki" className="team-card" /></th>
                                    <th className="text-center"><img src={redcard} title="Czerwone kartki" className="team-card" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="text-center match-page-header">
                                    <td colSpan={7}>Bramkarze</td>
                                </tr>
                                {team.squadPlayers && team.squadPlayers.length !== 0 && team.squadPlayers.map((player, index2) => (
                                    player.name === "Goalkeeper" &&
                                    <tr key={index2}>
                                        <td className="text-center" title="Numer koszulki">{player.number}</td>
                                        <td className="text-left"><div className="cell-inline" title={player.nationality}><FlagIcon code={getCode(player.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<Link to={`/player/${player.player_id}`} className="team-link">{player.common_name}</Link></td>
                                        <td className="text-center" title="Wiek">{player.age}</td>
                                        <td className="text-center" title="Bramki">{player.goals}</td>
                                        <td className="text-center" title="Asysty">{player.assists}</td>
                                        <td className="text-center" title="Żółte kartki">{player.yellowcards}</td>
                                        <td className="text-center" title="Czerwone kartki">{player.redcards}</td>
                                    </tr>))}
                                <tr className="text-center match-page-header">
                                    <td colSpan={7}>Obrońcy</td>
                                </tr>
                                {team.squadPlayers && team.squadPlayers.length !== 0 && team.squadPlayers.map((player, index2) => (
                                    player.name === "Defender" &&
                                    <tr key={index2}>
                                        <td className="text-center" title="Numer koszulki">{player.number}</td>
                                        <td className="text-left"><div className="cell-inline" title={player.nationality}><FlagIcon code={getCode(player.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<Link to={`/player/${player.player_id}`} className="team-link">{player.common_name}</Link></td>
                                        <td className="text-center" title="Wiek">{player.age}</td>
                                        <td className="text-center" title="Bramki">{player.goals}</td>
                                        <td className="text-center" title="Asysty">{player.assists}</td>
                                        <td className="text-center" title="Żółte kartki">{player.yellowcards}</td>
                                        <td className="text-center" title="Czerwone kartki">{player.redcards}</td>
                                    </tr>))}
                                <tr className="text-center match-page-header">
                                    <td colSpan={7}>Pomocnicy</td>
                                </tr>
                                {team.squadPlayers && team.squadPlayers.length !== 0 && team.squadPlayers.map((player, index2) => (
                                    player.name === "Midfielder" &&
                                    <tr key={index2}>
                                        <td className="text-center" title="Numer koszulki">{player.number}</td>
                                        <td className="text-left"><div className="cell-inline" title={player.nationality}><FlagIcon code={getCode(player.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<Link to={`/player/${player.player_id}`} className="team-link">{player.common_name}</Link></td>
                                        <td className="text-center" title="Wiek">{player.age}</td>
                                        <td className="text-center" title="Bramki">{player.goals}</td>
                                        <td className="text-center" title="Asysty">{player.assists}</td>
                                        <td className="text-center" title="Żółte kartki">{player.yellowcards}</td>
                                        <td className="text-center" title="Czerwone kartki">{player.redcards}</td>
                                    </tr>))}
                                <tr className="text-center match-page-header">
                                    <td colSpan={7}>Napastnicy</td>
                                </tr>
                                {team.squadPlayers && team.squadPlayers.length !== 0 && team.squadPlayers.map((player, index2) => (
                                    player.name === "Attacker" &&
                                    <tr key={index2}>
                                        <td className="text-center" title="Numer koszulki">{player.number}</td>
                                        <td className="text-left"><div className="cell-inline" title={player.nationality}><FlagIcon code={getCode(player.nationality).toLowerCase()} /></div>&nbsp;&nbsp;<Link to={`/player/${player.player_id}`} className="team-link">{player.common_name}</Link></td>
                                        <td className="text-center" title="Wiek">{player.age}</td>
                                        <td className="text-center" title="Bramki">{player.goals}</td>
                                        <td className="text-center" title="Asysty">{player.assists}</td>
                                        <td className="text-center" title="Żółte kartki">{player.yellowcards}</td>
                                        <td className="text-center" title="Czerwone kartki">{player.redcards}</td>
                                    </tr>))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { team, loading, error } = state.team;
    return {
        team,
        loading,
        error
    };
}

const connectedTeamPage = connect(mapStateToProps)(TeamPage);
export { connectedTeamPage as TeamPage }; 