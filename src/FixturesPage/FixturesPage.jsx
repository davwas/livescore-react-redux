import React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Nav, Navbar, NavItem } from 'react-bootstrap';
import Select from 'react-select';
import { fixturesActions } from '../_actions';
import { selectConstants, matchtypeConstants, fixturesConstants } from '../_constants';
import { dateOptions } from '../_helpers';

import './fixturespage.css';
import { FixturesAll } from './FixturesAll';
import { FixturesEnded } from './FixturesEnded';
import { FixturesLive } from './FixturesLive';

class FixturesPage extends React.Component {
    updateSelectOption(o) {
        this.props.dispatch({
            type: selectConstants.UPDATE_SELECT_OPTION,
            payload: o
        });
    }

    handleClickAll(e) {
        this.handleChangeShow();
        this.props.dispatch({
            type: matchtypeConstants.MATCH_TYPE_ALL
        });
        this.setState({ activeKey: 1 })
    }

    handleClickLive(e) {
        this.handleChangeHide();
        this.props.dispatch({
            type: selectConstants.UPDATE_SELECT_OPTION,
            payload: dateOptions[7]
        });
        this.props.dispatch({
            type: matchtypeConstants.MATCH_TYPE_LIVE
        });
        this.setState({ activeKey: 2 })
    }

    handleClickEnded(e) {
        this.handleChangeShow();
        this.props.dispatch({
            type: matchtypeConstants.MATCH_TYPE_ENDED
        });
        this.setState({ activeKey: 3 })
    }

    constructor() {
        super();

        this.state = {
            displaySelector: true,
            activeKey: 1
        };

        this.handleChangeShow = this.handleChangeShow.bind(this);
        this.handleChangeHide = this.handleChangeHide.bind(this);
    }

    handleChangeShow() {
        this.setState({ displaySelector: true });
    }

    handleChangeHide() {
        this.setState({ displaySelector: false });
    }

    render() {
        const style = this.state.displaySelector ? {} : { display: 'none' };
        const { matchesType } = this.props;
        const colourStyles = {
            option: (base, state) => ({
                ...base,
                backgroundColor: state.isSelected ? "limegreen" : state.isFocused ? "lightgreen" : "white"
            })
        };
        return (
            <Grid>
                <Row>
                    <Col xs={12} md={8} mdOffset={2}>
                        <Navbar inverse fluid className="navbar-fixtures">
                            <Nav activeKey={this.state.activeKey}>
                                <NavItem eventKey={1} onClick={this.handleClickAll.bind(this)}>
                                    Wszystkie
                                </NavItem>
                                <NavItem eventKey={2} onClick={this.handleClickLive.bind(this)}>
                                    Na żywo
                                </NavItem>
                                <NavItem eventKey={3} onClick={this.handleClickEnded.bind(this)}>
                                    Zakończone
                                </NavItem>
                            </Nav>
                            <Nav pullRight className="date-selector">
                                <div style={style}>
                                    <Select
                                        options={dateOptions}
                                        value={{ value: this.props.option, label: this.props.label }}
                                        onChange={this.updateSelectOption.bind(this)}
                                        isSearchable={false}
                                        isDisabled={false}
                                        styles={colourStyles}
                                    />
                                </div>
                            </Nav>
                        </Navbar>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={8} mdOffset={2}>
                        {matchesType === "all" && <FixturesAll />}
                        {matchesType === "ended" && <FixturesEnded />}
                        {matchesType === "live" && <FixturesLive />}
                    </Col>
                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { option, label } = state.select;
    const { matchesType } = state.matchtype;
    return {
        option,
        label,
        matchesType
    };
}

const connectedFixturesPage = connect(mapStateToProps)(FixturesPage);
export { connectedFixturesPage as FixturesPage }; 