import React from 'react';
import { connect } from 'react-redux';
import { fixturesActions, head2headActions } from '../_actions';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import FlagIcon from '../_components/FlagIcon'
import NewWindow from 'react-new-window';
import { Grid, Row, Col, Nav, NavItem } from 'react-bootstrap';
import { isIE, isEdge } from 'react-device-detect';
import './fixturespage.css';

class FixturesEnded extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMatchPage: false,
            matchInfo: null,
            activeKeyMatchPage: 1
        }
        this.toggleMatchPage = this.toggleMatchPage.bind(this);
    }
    handleClickDetails() {
        this.setState({ activeKeyMatchPage: 1 });
    }
    handleClickH2H() {
        this.setState({ activeKeyMatchPage: 2 });
        this.props.dispatch(head2headActions.fetchH2H(this.state.matchInfo.localteam_id, this.state.matchInfo.visitorteam_id));
    }
    toggleMatchPage(value) {
        this.setState(state => ({
            ...state,
            showMatchPage: !state.showMatchPage,
            matchInfo: value
        }));
    }
    closeMatchPage() {
        this.setState({
            showMatchPage: false,
            activeKeyMatchPage: 1
        })
    }
    componentDidMount() {
        if (!this.props.loading && !this.props.initialLoading) {
            this.props.dispatch(fixturesActions.fetchEndedMatchesInitial(this.props.option));
            this.timer = setInterval(() => this.props.dispatch(fixturesActions.fetchEndedMatches(this.props.option)), 60000);
        }
    }
    componentWillUnmount() {
        clearInterval(this.timer);
    }
    componentDidUpdate(prevProps) {
        if (this.props.option !== prevProps.option) {
            this.props.dispatch(fixturesActions.fetchEndedMatchesInitial(this.props.option));
        }
    }
    render() {
        const { error, items, initialLoading, h2h, h2h_loading, h2h_error } = this.props;
        const yellowcard = require("../_static/yellowcard.svg");
        const yellowred = require("../_static/yellowred.svg");
        const redcard = require("../_static/redcard.svg");
        const goal = require("../_static/soccerball.svg");
        const owngoal = require("../_static/soccerball1.svg");
        const uparrow = require("../_static/uparrow1.svg");
        const downarrow = require("../_static/downarrow1.svg");
        const thishostname = window.location.protocol + "//" + window.location.host + "/";
        if (error) {
            return <div className="text-center">Błąd API! {error.message}</div>;
        }
        if (initialLoading) {
            return <div className="text-center">
                <Loader
                    type="Triangle"
                    color="#006400"
                    height="100"
                    width="100"
                />
            </div>;
        }
        if (items.length === 0 || (items[0].length === 0 && items[1].length === 0 && items[2].length === 0 && items[3].length === 0 && items[4].length === 0)) {
            return <div className="text-center">W tym dniu nie ma żadnych zakończonych meczów.</div>;
        }
        return (
            <div>
                {items.map((country, index) => (
                    country.length !== 0 &&
                    <Table hover responsive key={index}>
                        <thead>
                            <tr>
                                <th colSpan={7} className="table-header"><FlagIcon code={country[0].countryCode} className="flag"></FlagIcon>{[country[0].countryName, " - ", country[0].leagueName]}</th>
                            </tr>
                        </thead>
                        {country.map((match, index2) => (
                            <tbody className="text-center cursor-pointer" key={index2}>
                                <tr onClick={() => this.toggleMatchPage(match)}>
                                    <td className="table-column-1">{match.matchTime}</td>
                                    <td className="table-column-2"></td>
                                    <td className="table-column-3">{match.matchStatusPL}</td>
                                    <td className="table-column-4 text-right">
                                        {match.homeTeam.homeTeamRedCards === 1 ? <img src={redcard} title="Czerwona kartka" className="card-home" /> :
                                            match.homeTeam.homeTeamRedCards === 2 ? [<img src={redcard} title="Czerwona kartka" className="card-home" />, <img src={redcard} title="Czerwona kartka" className="card-home" />] :
                                                match.homeTeam.homeTeamRedCards === 3 ? [<img src={redcard} title="Czerwona kartka" className="card-home" />, <img src={redcard} title="Czerwona kartka" className="card-home" />, <img src={redcard} title="Czerwona kartka" className="card-home" />] : ""}
                                        {match.homeTeam.name === match.matchWinner ? <strong>{match.homeTeam.name}</strong> : match.homeTeam.name}
                                    </td>
                                    <td className="table-column-5"><strong>{[match.homeTeam.localteam_score, " - ", match.awayTeam.visitorteam_score]}</strong></td>
                                    <td className="table-column-6 text-left">
                                        {match.awayTeam.name === match.matchWinner ? <strong>{match.awayTeam.name}</strong> : match.awayTeam.name}
                                        {match.awayTeam.awayTeamRedCards === 1 ? <img src={redcard} title="Czerwona kartka" className="card-away" /> :
                                            match.awayTeam.awayTeamRedCards === 2 ? [<img src={redcard} title="Czerwona kartka" className="card-away" />, <img src={redcard} title="Czerwona kartka" className="card-away" />] :
                                                match.awayTeam.awayTeamRedCards === 3 ? [<img src={redcard} title="Czerwona kartka" className="card-away" />, <img src={redcard} title="Czerwona kartka" className="card-away" />, <img src={redcard} title="Czerwona kartka" className="card-away" />] : ""}
                                    </td>
                                    <td className="table-column-7" title="Wynik pierwszej połowy">{match.ht_score}</td>
                                </tr>
                            </tbody>))}
                    </Table>
                ))}
                {this.state.showMatchPage && !isIE && !isEdge && (
                    <NewWindow center="" features={{ width: '640px', height: '700px', top: '100px', left: '100px' }} onUnload={this.closeMatchPage.bind(this)} title={this.state.matchInfo.homeTeam.name + " - " + this.state.matchInfo.awayTeam.name + " | Szczegóły"}>
                        <Grid className="match-page-teams">
                            <Row>
                                <Col xs={12} className="match-page-timedate"><div>{this.state.matchInfo.date}</div><div>{this.state.matchInfo.matchTime}</div></Col>
                            </Row>
                            <Row className="match-page-header">
                                <Col xs={12}>{[this.state.matchInfo.countryName, " - ", this.state.matchInfo.leagueName]}</Col>
                            </Row>
                            <Row className="match-page-info">
                                <Col xs={4}><img className="logo" src={this.state.matchInfo.homeTeam.logo_path} /><br /><strong><Link className="team-link" to={`/team/${this.state.matchInfo.localteam_id}`}>{this.state.matchInfo.homeTeam.name}</Link></strong></Col>
                                <Col xs={4} className="match-page-score">{this.state.matchInfo.status === "NS" ? " - " : (this.state.matchInfo.status === "LIVE" || this.state.matchInfo.status === "HT") ? <strong className="live">{[this.state.matchInfo.homeTeam.localteam_score, " - ", this.state.matchInfo.awayTeam.visitorteam_score]}</strong> : <strong>{[this.state.matchInfo.homeTeam.localteam_score, " - ", this.state.matchInfo.awayTeam.visitorteam_score]}</strong>}</Col>
                                <Col xs={4}><img className="logo" src={this.state.matchInfo.awayTeam.logo_path} /><br /><strong><Link className="team-link" to={`/team/${this.state.matchInfo.visitorteam_id}`}>{this.state.matchInfo.awayTeam.name}</Link></strong></Col>
                            </Row>
                        </Grid>
                        <Table hover responsive>
                            <thead>
                                <tr>
                                    <th className="text-center" colSpan={4}>
                                        <Nav activeKey={this.state.activeKeyMatchPage} bsStyle="tabs">
                                            <NavItem eventKey={1} onClick={this.handleClickDetails.bind(this)} className="inline-block">Szczegóły</NavItem>
                                            <NavItem eventKey={2} onClick={this.handleClickH2H.bind(this)} className="inline-block">Head2Head</NavItem>
                                        </Nav>
                                    </th>
                                </tr>
                            </thead>
                            {this.state.activeKeyMatchPage === 1 ?
                                <tbody>
                                    {this.state.matchInfo.eventslist.length === 0 && <tr className="text-center"><td colSpan={3}>Brak szczegółowych danych.</td></tr>}
                                    {this.state.matchInfo.eventslist.length !== 0 && <tr className="text-center match-page-header"><td colSpan={3}>1. połowa</td></tr>}
                                    {this.state.matchInfo.eventslist.map((event, index3) => (
                                        (event.minute <= 45 || (event.minute <= 55 && event.extra_minute !== null)) && <tr key={index3}>
                                            <td className="match-page-left">
                                                {this.state.matchInfo.homeTeam.name === event.teamName && (
                                                    event.type === "goal" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + goal} title="Bramka" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline related-player" title="Asystujący">&nbsp;&nbsp;<Link to={`/player/${event.related_player_id}`} className="related-player">{event.related_player_name}</Link></div></div> :
                                                        event.type === "penalty" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + goal} title="Rzut karny" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline goal-type"> (Rzut Karny)</div></div> :
                                                            event.type === "own-goal" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + owngoal} title="Bramka samobójcza" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline goal-type"> (Bramka samobójcza)</div></div> :
                                                                event.type === "yellowcard" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + yellowcard} title="Żółta kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div></div> :
                                                                    event.type === "yellowred" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + yellowred} title="Druga żółta/czerwona kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div></div> :
                                                                        event.type === "redcard" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong><img src={thishostname + redcard} title="Czerwona kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div></div> :
                                                                            event.type === "substitution" ? <div><div className="match-page-inline"><strong>{event.minute}' </strong></div><div className="match-page-inline"> <img src={thishostname + uparrow} title="Wchodzi" className="arrow" /> <Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline related-player"> <img src={thishostname + downarrow} title="Schodzi" className="arrow" /> <Link to={`/player/${event.related_player_id}`} className="related-player">{event.related_player_name}</Link></div></div> : "")}
                                            </td>
                                            <td></td>
                                            <td className="match-page-right">
                                                {this.state.matchInfo.awayTeam.name === event.teamName && (
                                                    event.type === "goal" ? <div><div className="match-page-inline related-player" title="Asystujący"><Link to={`/player/${event.related_player_id}`} className="related-player">{event.related_player_name}</Link>&nbsp;&nbsp;</div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + goal} title="Bramka" className="ball" /> <strong>{event.minute}'</strong></div></div> :
                                                        event.type === "penalty" ? <div><div className="match-page-inline goal-type">(Rzut karny) </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + goal} title="Rzut karny" className="ball" /> <strong>{event.minute}'</strong></div></div> :
                                                            event.type === "own-goal" ? <div><div className="match-page-inline goal-type">(Bramka samobójcza) </div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + owngoal} title="Bramka samobójcza" className="ball" /> <strong>{event.minute}'</strong></div></div> :
                                                                event.type === "yellowcard" ? <div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + yellowcard} title="Żółta kartka" className="card" /> <strong>{event.minute}'</strong></div></div> :
                                                                    event.type === "yellowred" ? <div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + yellowred} title="Druga żółta/czerwona kartka" className="card" /> <strong>{event.minute}'</strong></div></div> :
                                                                        event.type === "redcard" ? <div><div className="match-page-inline"><Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + redcard} title="Czerwona kartka" className="card" /> <strong>{event.minute}'</strong></div></div> :
                                                                            event.type === "substitution" ? <div><div className="match-page-inline related-player"> <img src={thishostname + downarrow} title="Schodzi" className="arrow" /> <Link to={`/player/${event.related_player_id}`} className="related-player">{event.related_player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + uparrow} title="Wchodzi" className="arrow" /> <Link to={`/player/${event.player_id}`} className="team-link">{event.player_name}</Link></div><div className="match-page-inline"> <strong>{event.minute}'</strong></div></div> : "")}
                                            </td>
                                        </tr>
                                    ))}
                                    {this.state.matchInfo.eventslist.length !== 0 && <tr className="text-center match-page-header"><td colSpan={3}>2. połowa</td></tr>}
                                    {this.state.matchInfo.eventslist.map((event2, index4) => (
                                        ((event2.minute >= 45 && event2.extra_minute === null) || event2.minute > 55) && <tr key={index4}>
                                            <td className="match-page-left">
                                                {this.state.matchInfo.homeTeam.name === event2.teamName && (
                                                    event2.type === "goal" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + goal} title="Bramka" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline related-player" title="Asystujący">&nbsp;&nbsp;<Link to={`/player/${event2.related_player_id}`} className="related-player">{event2.related_player_name}</Link></div></div> :
                                                        event2.type === "penalty" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + goal} title="Rzut karny" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline goal-type"> (Rzut Karny)</div></div> :
                                                            event2.type === "own-goal" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + owngoal} title="Bramka samobójcza" className="ball" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline goal-type"> (Bramka samobójcza)</div></div> :
                                                                event2.type === "yellowcard" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + yellowcard} title="Żółta kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div></div> :
                                                                    event2.type === "yellowred" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + yellowred} title="Druga żółta/czerwona kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div></div> :
                                                                        event2.type === "redcard" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong><img src={thishostname + redcard} title="Czerwona kartka" className="card" /> </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div></div> :
                                                                            event2.type === "substitution" ? <div><div className="match-page-inline"><strong>{event2.minute}' </strong></div><div className="match-page-inline"> <img src={thishostname + uparrow} title="Wchodzi" className="arrow" /> <Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline related-player"> <img src={thishostname + downarrow} title="Schodzi" className="arrow" /> <Link to={`/player/${event2.related_player_id}`} className="related-player">{event2.related_player_name}</Link></div></div> : "")}
                                            </td>
                                            <td></td>
                                            <td className="match-page-right">
                                                {this.state.matchInfo.awayTeam.name === event2.teamName && (
                                                    event2.type === "goal" ? <div><div className="match-page-inline related-player" title="Asystujący"><Link to={`/player/${event2.related_player_id}`} className="related-player">{event2.related_player_name}</Link>&nbsp;&nbsp;</div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + goal} title="Bramka" className="ball" /> <strong>{event2.minute}'</strong></div></div> :
                                                        event2.type === "penalty" ? <div><div className="match-page-inline goal-type">(Rzut karny) </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + goal} title="Rzut karny" className="ball" /> <strong>{event2.minute}'</strong></div></div> :
                                                            event2.type === "own-goal" ? <div><div className="match-page-inline goal-type">(Bramka samobójcza) </div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + owngoal} title="Bramka samobójcza" className="ball" /> <strong>{event2.minute}'</strong></div></div> :
                                                                event2.type === "yellowcard" ? <div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + yellowcard} title="Żółta kartka" className="card" /> <strong>{event2.minute}'</strong></div></div> :
                                                                    event2.type === "yellowred" ? <div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + yellowred} title="Druga żółta/czerwona kartka" className="card" /> <strong>{event2.minute}'</strong></div></div> :
                                                                        event2.type === "redcard" ? <div><div className="match-page-inline"><Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + redcard} title="Czerwona kartka" className="card" /> <strong>{event2.minute}'</strong></div></div> :
                                                                            event2.type === "substitution" ? <div><div className="match-page-inline related-player"> <img src={thishostname + downarrow} title="Schodzi" className="arrow" /> <Link to={`/player/${event2.related_player_id}`} className="related-player">{event2.related_player_name}</Link></div><div className="match-page-inline"> <img src={thishostname + uparrow} title="Wchodzi" className="arrow" /> <Link to={`/player/${event2.player_id}`} className="team-link">{event2.player_name}</Link></div><div className="match-page-inline"> <strong>{event2.minute}'</strong></div></div> : "")}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody> :
                                h2h_loading ?
                                    <tbody>
                                        <tr colSpan={4} className="text-center">
                                            <td>
                                                <Loader
                                                    type="Triangle"
                                                    color="#006400"
                                                    height="100"
                                                    width="100"
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                    :
                                    h2h_error ?
                                        <tbody>
                                            <tr colSpan={4} className="text-center">
                                                <td>
                                                    Błąd API! {h2h_error.message}
                                                </td>
                                            </tr>
                                        </tbody> :
                                        <tbody>
                                            <tr className="text-center match-page-header"><td colSpan={4}>Ostatnie spotkania</td></tr>
                                            {h2h.map((prevgame, index5) => (
                                                <tr key={index5}>
                                                    <td className="time">{prevgame.date}</td>
                                                    <td className="text-right">{prevgame.matchWinner === prevgame.homeTeamInfo.name ? <strong>{prevgame.homeTeamInfo.name}</strong> : prevgame.homeTeamInfo.name}</td>
                                                    <td className="text-center"><strong>{prevgame.homeTeamInfo.localteam_score} - {prevgame.awayTeamInfo.visitorteam_score}</strong></td>
                                                    <td className="text-left">{prevgame.matchWinner === prevgame.awayTeamInfo.name ? <strong>{prevgame.awayTeamInfo.name}</strong> : prevgame.awayTeamInfo.name}</td>
                                                </tr>
                                            ))}
                                        </tbody>}
                        </Table>
                    </NewWindow>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { option } = state.select;
    const { items, loading, error, isCached, initialLoading } = state.fixtures;
    const { h2h, h2h_loading, h2h_error } = state.head2head;
    return {
        option,
        items,
        loading,
        error,
        isCached,
        initialLoading,
        h2h,
        h2h_loading,
        h2h_error
    };
}

const connectedFixturesEnded = connect(mapStateToProps)(FixturesEnded);
export { connectedFixturesEnded as FixturesEnded }; 