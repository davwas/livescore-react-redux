export const matchtypeConstants = {
    MATCH_TYPE_ALL: 'MATCH_TYPE_ALL',
    MATCH_TYPE_LIVE: 'MATCH_TYPE_LIVE',
    MATCH_TYPE_ENDED: 'MATCH_TYPE_ENDED'
};