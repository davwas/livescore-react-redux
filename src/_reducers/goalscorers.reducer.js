import { goalscorersConstants } from '../_constants';

const initialState = {
    topscorers: [],
    loading: false,
    error: null,
    isCached: false,
};

export function goalscorers(state = initialState, action) {
    switch (action.type) {
        case goalscorersConstants.FETCH_GOALSCORERS_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case goalscorersConstants.FETCH_GOALSCORERS_SUCCESS:
            return {
                ...state,
                loading: false,
                topscorers: action.payload.topscorers,
                isCached: true
            };
        case goalscorersConstants.FETCH_GOALSCORERS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}