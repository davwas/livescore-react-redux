import { assistscorersConstants } from '../_constants';

const initialState = {
    topassistants: [],
    loading: false,
    error: null,
    isCached: false,
};

export function assistscorers(state = initialState, action) {
    switch (action.type) {
        case assistscorersConstants.FETCH_ASSISTSCORERS_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case assistscorersConstants.FETCH_ASSISTSCORERS_SUCCESS:
            return {
                ...state,
                loading: false,
                topassistants: action.payload.topassistants,
                isCached: true
            };
        case assistscorersConstants.FETCH_ASSISTSCORERS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}