import { leaguestatsConstants } from '../_constants';

const initialState = {
    teams: [],
    loading: false,
    error: null,
    isCached: false,
};

export function leaguestats(state = initialState, action) {
    switch (action.type) {
        case leaguestatsConstants.FETCH_LEAGUESTATS_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case leaguestatsConstants.FETCH_LEAGUESTATS_SUCCESS:
            return {
                ...state,
                loading: false,
                teams: action.payload.teams,
                isCached: true
            };
        case leaguestatsConstants.FETCH_LEAGUESTATS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}