import { playerConstants } from '../_constants';

const initialState = {
    player: [],
    loading: false,
    error: null,
    isCached: false,
};

export function player(state = initialState, action) {
    switch (action.type) {
        case playerConstants.FETCH_PLAYER_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case playerConstants.FETCH_PLAYER_SUCCESS:
            return {
                ...state,
                loading: false,
                player: action.payload.player,
                isCached: true
            };
        case playerConstants.FETCH_PLAYER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}