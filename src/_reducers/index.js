import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { select } from './select.reducer';
import { matchtype } from './matchtype.reducer';
import { fixtures } from './fixtures.reducer';
import { leaguestats } from './leaguestats.reducer';
import { goalscorers } from './goalscorers.reducer';
import { assistscorers } from './assistscorers.reducer';
import { head2head } from './head2head.reducer';
import { team } from './team.reducer';
import { player } from './player.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  select,
  matchtype,
  fixtures,
  leaguestats,
  goalscorers,
  assistscorers,
  head2head,
  team,
  player
});

export default rootReducer;