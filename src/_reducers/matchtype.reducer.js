import { matchtypeConstants } from '../_constants';

const initialState = {
    matchesType: "all"
};

export function matchtype(state = initialState, action) {
    switch (action.type) {
        case matchtypeConstants.MATCH_TYPE_ALL:
            return {
                ...state,
                matchesType: "all"
            };
        case matchtypeConstants.MATCH_TYPE_LIVE:
            return {
                ...state,
                matchesType: "live"
            };
        case matchtypeConstants.MATCH_TYPE_ENDED:
            return {
                ...state,
                matchesType: "ended"
            };
        default:
            return state;
    }
}