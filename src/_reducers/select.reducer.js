import { selectConstants } from '../_constants';

let date = new Date();
const today = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();

const initialState = {
    option: today,
    label: "Dzisiaj"
};

export function select(state = initialState, action) {
    switch (action.type) {
        case selectConstants.UPDATE_SELECT_OPTION:
            const option = action.payload.value;
            const label = action.payload.label;
            return {
                ...state,
                option,
                label
            };
        default:
            return state;
    }
}