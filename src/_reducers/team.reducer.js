import { teamConstants } from '../_constants';

const initialState = {
    team: [],
    loading: false,
    error: null,
    isCached: false,
};

export function team(state = initialState, action) {
    switch (action.type) {
        case teamConstants.FETCH_TEAM_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case teamConstants.FETCH_TEAM_SUCCESS:
            return {
                ...state,
                loading: false,
                team: action.payload.team,
                isCached: true
            };
        case teamConstants.FETCH_TEAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}