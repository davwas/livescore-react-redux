import { head2headConstants } from '../_constants';

const initialState = {
    h2h: [],
    h2h_loading: false,
    h2h_error: null,
    h2h_isCached: false,
};

export function head2head(state = initialState, action) {
    switch (action.type) {
        case head2headConstants.FETCH_H2H_BEGIN:
            return {
                ...state,
                h2h_loading: true,
                h2h_error: null,
                isCached: false
            };
        case head2headConstants.FETCH_H2H_SUCCESS:
            return {
                ...state,
                h2h_loading: false,
                h2h: action.payload.h2h,
                h2h_isCached: true
            };
        case head2headConstants.FETCH_H2H_FAILURE:
            return {
                ...state,
                h2h_loading: false,
                h2h_error: action.payload.error,
                h2h_isCached: false
            };
        default:
            return state;
    }
}