import { fixturesConstants } from '../_constants';

const initialState = {
    items: [],
    loading: false,
    error: null,
    isCached: false,
    initialLoading: false,
};

export function fixtures(state = initialState, action) {
    switch (action.type) {
        case fixturesConstants.FETCH_MATCHES_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
                isCached: false
            };
        case fixturesConstants.FETCH_MATCHES_INITIAL_BEGIN:
            return {
                ...state,
                initialLoading: true,
                error: null,
                isCached: false
            };
        case fixturesConstants.FETCH_MATCHES_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.matches,
                isCached: true
            };
        case fixturesConstants.FETCH_MATCHES_INITIAL_SUCCESS:
            return {
                ...state,
                initialLoading: false,
                items: action.payload.matches,
                isCached: true
            };
        case fixturesConstants.FETCH_MATCHES_FAILURE:
            return {
                ...state,
                items: [],
                loading: false,
                error: action.payload.error,
                isCached: false
            };
        case fixturesConstants.FETCH_MATCHES_INITIAL_FAILURE:
            return {
                ...state,
                items: [],
                initialLoading: false,
                error: action.payload.error,
                isCached: false
            };
        default:
            return state;
    }
}