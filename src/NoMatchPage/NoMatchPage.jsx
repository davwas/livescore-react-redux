import React from 'react';
import { connect } from 'react-redux';
import { Jumbotron } from 'react-bootstrap';
import './nomatchpage.css';

class NoMatchPage extends React.Component {
    render() {
        return (
            <Jumbotron className="not-found">
                <h1>#404</h1>
                <p>Nie znaleziono strony.</p>
            </Jumbotron>
        )
    }
}

const connectedNoMatchPage = connect()(NoMatchPage);
export { connectedNoMatchPage as NoMatchPage }; 