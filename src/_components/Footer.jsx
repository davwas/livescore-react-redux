import React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './footer.css';

class Footer extends React.Component {
    render() {
        const { loggedIn, user } = this.props;
        return (
            <div className="footer">
                <Grid>
                    <Row className="text-center vdivide">
                        <Col sm={4} className="fix-line">
                            <h2>Livescore</h2>
                        </Col>
                        <Col sm={4}>
                            <div>
                                Przydatne linki:
                                <div>{(user && loggedIn) ? <Link to="/login">Wyloguj się</Link> : <Link to="/login">Logowanie</Link>}</div>
                                <div><Link to="/register">Rejestracja</Link></div>
                                <div><Link to="/stats/premierleague">Premier League</Link></div>
                            </div>
                        </Col>
                        <Col sm={4}>
                            <p className="footnote">Aplikacja webowa do prezentowania wyników piłkarskich na żywo oraz statystyk.</p>
                        </Col>
                    </Row>
                    <Row className="text-center author-name">
                        &copy; Dawid Wasik
                </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggedIn, user } = state.authentication;
    return {
        loggedIn,
        user
    };
}

const connectedFooter = connect(mapStateToProps)(Footer);

export { connectedFooter as Footer }; 