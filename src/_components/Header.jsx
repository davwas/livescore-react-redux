import React from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './header.css';

class Header extends React.Component {
    render() {
        const { user, loggedIn } = this.props;
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        Livescore
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer exact to="/">
                            <NavItem>Mecze</NavItem>
                        </LinkContainer>
                        <LinkContainer exact to="/stats">
                            <NavItem>Statystyki</NavItem>
                        </LinkContainer>
                    </Nav>
                    {loggedIn ?
                        <Nav pullRight>
                            <NavItem className="logged-in">
                                Witaj, {user.username}
                            </NavItem>
                            <LinkContainer exact to="/login">
                                <NavItem>Wyloguj się</NavItem>
                            </LinkContainer>
                        </Nav>
                        :
                        <Nav pullRight>
                            <LinkContainer exact to="/login">
                                <NavItem>Zaloguj się</NavItem>
                            </LinkContainer>
                            <LinkContainer exact to="/register">
                                <NavItem>Zarejestruj się</NavItem>
                            </LinkContainer>
                        </Nav>}
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

function mapStateToProps(state) {
    const { authentication } = state;
    const { user, loggedIn } = authentication;
    return {
        user,
        loggedIn
    };
}

const connectedHeader = connect(mapStateToProps, null, null, { pure: false })(Header);

export { connectedHeader as Header }; 